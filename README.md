# freck

*freck* is the bootstrap script for [*freckles*](https://freckles.io) that optionally supports the immediate execution
of any of the applications that come with the *freckles* package. 

```console
# install freckles
curl https://freckles.sh | bash

# download (if necessary) and execute, without adding it to your $PATH
curl https://freckles.sh | bash -s -- <application> <args>
```

You can use it from it's 'official' url (https://freckles.sh), or download it, optionally customize it (change defaults, add your own aliases, etc.), and host it yourself somewhere.

## Usage

### cli

Here's how the commandline interface looks on a high level:

```console

<curl_or_wget_command> https://freckles.sh | ENV_KEY_1=<env_value_1> ENV_KEY_2=<env_value> bash -s -- <application> <app_args>
|                    | |                   |   |                                           |           |                      |
 - download command -   ------- url -------     ---------- control behaviour --------------             ---- app execution ---

```

And here is what the individual items mean (for more details, check out the [freck script](/freck) itself):

- ``download command``: either ``curl`` (or use ``curl -s`` if you don't want to see its progress), or ``wget -O -``
- ``url``: always ``https://freckles.sh`` (you can also use ``https://freckles.io`` if you want, though)
- ``control behaviour``: 
  - ``CLEANUP``: removes 'freckles' from the system after execution (if an application was provided/no install) (default: 'false')
  - ``SILENT``: when set to 'true', disables all output except for error messages (default: 'false')
  - ``ADD_TO_INIT``: list of (init) files to add freckles init code to (default: 'default')
  - ``NO_ADD_TO_INIT``: prevents adding init code to any files (even if 'ADD_TO_INIT' is set) (default: false)
- ``app execution``: this is the same you'd use if you would execute the application if it was already installed and available in your ``PATH``, for example: ``ansible-playbook --ask-become-pass play.yml``


### 'getting' *freck*


As already mentioned, you can either use ``curl`` or ``wget`` to download *freck*. Actually, any other tool you have at hand that can download files from the internet, and pipe out their content. I focus on ``curl`` and ``wget`` since the likelyhood one of them being installed is highest.

### curl

As mentioned above, this is how to invoke *freck* using ``curl``:

```
curl https://freckles.sh | bash -s -- <app_name> <app_args>>
```

### wget

And using ``wget``:

```
wget -O - https://freckles.sh | bash -s -- <app_name> <app_args>
```

For the following examples I'll always use ``curl``, but of course you can use ``wget`` interchangeably.

### alternative for interactive command


In case the command you are trying to inaugurate requires interactive input, it is safer to use either one of the following formats:

```
bash <(wget -O- https://freckles.sh) -- <app_name> <app_args>
```

or

```
bash <(curl -s https://freckles.sh) -- <app_name> <app_args>
```

## How does this work? What does it do?

*freck* touches two things when it is run. If run without arguments, it adds a small block of init code to ``$HOME/.profile`` (and, if they exist, ``$HOME/.zprofile`` and ``$HOME/.bash_profile``, or any of the other files specified in the ``ADD_TO_INIT`` environment variable) -- really only to put *freckles* in the ``$PATH``. And it creates a folder ``$HOME/.local/share/freckles`` where it puts all the application data it installs. 

### ``profile``/``.zprofile``/``.bash_profile``


By default, *inaugurate* adds those lines to the file in question (in case of ``$HOME/.profile, it will be created if it doesn't exist):

```bash
# begin: freckles init" >> "${path}"
export FRECKLES_PATH="${HOME}/.local/share/freckles"
if [ -d "$FRECKLES_PATH" ]; then
    PATH="$PATH:$FRECKLES_PATH"
fi
# end: freckles init
```

Obviously, this won't be in effect after your first *freck* run, as the ``.profile`` file wasn't read since then. You can 'force' picking up the new ``PATH`` by either logging out and logging in again, or sourcing ``.profile``:

```
source $HOME/.profile
```

Adding the *freckles* path to ``.profile`` can be disabled by specifying the ``NO_ADD_TO_INIT`` environment variable when running *freck*:

```
curl https://freckles.sh | NO_ADD_TO_INIT=true bash 
```

### ``$HOME/.local/share/freckles``

This is the default *freckles* application data folder, the *freckles* executable will be downloaded into ``$HOME/.local/share/freckles/bin``. In addition, ``freck`` will symlink the ``freckles`` application to the ``frecklecute`` name. 

## Is this secure?

What? Downloading and executing a random script from the internet? Duh.

That being said, you can download the [freck script](https://gitlab.com/freckles-io/freck/raw/develop/freck) manually, and host it yourself on Github (or somewhere else).

I'm not particularly interested to have the old 'curly bootstrap scripts are evil'-discussion, here are a few links to arguments already made, and fights already fought:

- https://news.ycombinator.com/item?id=12766049
- https://sandstorm.io/news/2015-09-24-is-curl-bash-insecure-pgp-verified-install
